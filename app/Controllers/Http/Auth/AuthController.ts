import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import AuthService from './AuthService'
import AuthValidation from './AuthValidation'
const authService = new AuthService
const authRules = new AuthValidation
export default class AuthController {
    public async registerUser({request}:HttpContextContract){
        const validateUserData = await authRules.validateRegisterData(request)
        return authService.createUser(validateUserData)
    }


}
