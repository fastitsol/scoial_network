import User from 'App/Models/User'
export default class AuthQuery {
    createUser(user : object) : object{
       return User.create(user)
    }
    getTotalCountFromUser(columnName, value){
       return User.query().where(columnName, value).count("* as total")
    }
};
