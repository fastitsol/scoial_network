import AuthQuery from './AuthQuery'
const authQuery = new AuthQuery
export default class AuthService {
    async createUser(user) {
      let userName = `${user.first_name}_${user.last_name}`
      let totalUserNames = await authQuery.getTotalCountFromUser('email', userName)

      if(totalUserNames[0].total == 1){
        userName = userName + '_' + totalUserNames[0].total
      }
      if(totalUserNames[0].total > 1){
        let newNumber : number = totalUserNames[0].total + 1
        userName = userName + '_' + newNumber
      }

      user.user_name = userName
      return authQuery.createUser(user)
    }
};
