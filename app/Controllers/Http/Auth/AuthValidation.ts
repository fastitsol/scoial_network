import { rules,schema} from '@ioc:Adonis/Core/Validator'
export default class AuthValidation {
    registerRules(){
      return schema.create({
          first_name: schema.string({trim:true},[
              rules.maxLength(255),
          ]),
          last_name: schema.string({trim:true},[
              rules.maxLength(255),
          ]),
          gender: schema.string({trim:true},[
              rules.maxLength(255),
          ]),
          email:schema.string({trim: true},[
              rules.email(),
              rules.maxLength(255),
              rules.unique({table: 'users', column: 'email'}),
          ]),
          password: schema.string({trim:true},[
              rules.minLength(6),
          ]),
      })
    }
    async validateRegisterData(request) : Promise<object>{
      const validateData = await request.validate({
          schema: this.registerRules(),
          messages:{
              'first_name.required' : 'Firstname is required',
              'last_name.required' : 'Lastname is required',
              'gender.required' : 'Gender is required',
              'email.required' : 'Email is required',
              'email.unique':'This email already in use. Choose a different one.',
              'password.required' : 'Password is required',
              'password.minLength' : 'Password must be at least 6 characters long',
              'password_confirmation.confirmed':'The password confirmation doesn`t match.',
          }
      })
      return validateData
    }
};
