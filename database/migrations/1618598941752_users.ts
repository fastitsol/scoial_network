import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Users extends BaseSchema {
  protected tableName = 'users'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('first_name').notNullable()
      table.string('last_name').notNullable()
      table.string('user_name').unique().notNullable()
      table.string('email').unique().notNullable()
      table.string('password').notNullable()
      table.string('status').defaultTo('active')
      table.string('verify_code')
      table.string('forgot_code')
      table.string('gender').notNullable()
      table.timestamps(true, true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
