import Route from '@ioc:Adonis/Core/Route'

Route.group(() =>{
  Route.post('/register', 'Auth/AuthController.registerUser')

}).prefix('auth')


